// @flow

import { wdp, hdp } from '../utils';

/**
 * Variables constants for spacing, fontSize and iconSize. Will be used to have a different sizes based on the device the app is running on.
 *
 * Values for the iPhone 8 are:
 *
 * `TINY: 10`
 *
 * `EXTRA_SMALL: 12`
 *
 * `SMALL: 14`
 *
 * `MEDIUM: 16`
 *
 * `LARGE: 18`
 *
 * `EXTRA_LARGE: 20`
 *
 * `HUGE: 24`
 *
 * By using this (which in turn is using the `wdp`, `hdp` functions), a `fontSize of 16` on the iPhone 8 will be `~18` on the iPhone XS Max,
 * which helps with the reponsive design of the app across different devices.
 */
export const Variables = {
	SpacingVertical: {
		TINY: hdp(1.5),
		EXTRA_SMALL: hdp(1.8),
		SMALL: hdp(2.1),
		MEDIUM: hdp(2.4),
		LARGE: hdp(2.7),
		EXTRA_LARGE: hdp(3),
		HUGE: hdp(3.6),
	},
	SpacingHorizontal: {
		TINY: wdp(2.7),
		EXTRA_SMALL: wdp(3.2),
		SMALL: wdp(3.7),
		MEDIUM: wdp(4.2),
		LARGE: wdp(4.8),
		EXTRA_LARGE: wdp(5.3),
		HUGE: wdp(6.4),
	},
	FontSize: {
		TINY: wdp(2.7),
		EXTRA_SMALL: wdp(3.2),
		SMALL: wdp(3.7),
		MEDIUM: wdp(4.2),
		LARGE: wdp(4.8),
		EXTRA_LARGE: wdp(5.3),
		HUGE: wdp(6.4),
	},
	IconSize: {
		TINY: wdp(4.2), // 16
		SMALL: wdp(4.8), // 18
		MEDIUM: wdp(5.3), // 20
		LARGE: wdp(6.4), // 24
	},
};
