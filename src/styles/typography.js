// @flow

import { Variables } from './variables';
import { Colors } from './colors';

export const Typography = {
	Text: {
		TINY: {
			fontSize: Variables.FontSize.TINY,
			color: Colors.BLACK,
		},
		EXTRA_SMALL: {
			fontSize: Variables.FontSize.EXTRA_SMALL,
			color: Colors.BLACK,
		},
		SMALL: {
			fontSize: Variables.FontSize.SMALL,
			color: Colors.BLACK,
		},
		MEDIUM: {
			fontSize: Variables.FontSize.MEDIUM,
			color: Colors.BLACK,
		},
		LARGE: {
			fontSize: Variables.FontSize.LARGE,
			color: Colors.BLACK,
		},
	},
};
