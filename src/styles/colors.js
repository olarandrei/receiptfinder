// @flow

export const Colors = {
	WHITE: '#FFF',
	BLACK: '#000',

	PRIMARY: '#f39c12',
	GRAY: 'rgb(245, 245, 245)',
};
