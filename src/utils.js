// @flow

import { PixelRatio, Dimensions } from 'react-native';

/**
 * Small wrapper over the default fetch method, so we can avoid doing `response.json()` each time we call `fetch`.
 *
 * @param {string} url The url
 */
export function doNetworkRequest(url: string): Promise<any> {
	return new Promise((resolve, reject) => {
		return fetch(url)
			.then(res => res.json())
			.then(res => resolve(res))
			.catch(err => reject(err));
	});
}

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

/**
 * Takes the percentage of the screen's width as an argument and returns the number of DPs.
 * Example: For a screenWidth of 300dp, wdp(100) will return 300, wdp(50) will return 150.
 *
 * @param {number} percentage Percentage of screen's width
 */
export const wdp = (percentage: number) => PixelRatio.roundToNearestPixel((screenWidth * percentage) / 100);

/**
 * Takes the percentage of the screen's height as an argument and returns the number of DPs.
 * Example: For a screenHeight of 600, hdp(100) will return 600, hdp(50) will return 300.
 *
 * @param {number} percentage Percentage of screen's height
 */
export const hdp = (percentage: number) => PixelRatio.roundToNearestPixel((screenHeight * percentage) / 100);
