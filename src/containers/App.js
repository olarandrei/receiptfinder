// @flow

import * as React from 'react';
import { View, SafeAreaView, StyleSheet } from 'react-native';

import type { Recipe } from '../types';

import { Colors } from '../styles/colors';
import { Variables } from '../styles/variables';
import { Header, RecipeSearch, RecipeDetails, EmptyPlaceholder } from '../components';

const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	content: {
		flex: 1,
		backgroundColor: Colors.WHITE,
		margin: Variables.SpacingHorizontal.MEDIUM,
	},
});

function App() {
	const [selectedRecipe, setSelectedRecipe] = React.useState<?Recipe>(undefined);

	const onRecipeSelect = React.useCallback((recipe) => setSelectedRecipe(recipe), []);

	return (
		<SafeAreaView style={styles.container}>
			<Header />
			<View style={styles.content}>
				<RecipeSearch onRecipeSelect={onRecipeSelect} />
				{selectedRecipe ? (
					<RecipeDetails recipe={selectedRecipe} />
				) : (
					<EmptyPlaceholder />
				)}
			</View>
		</SafeAreaView>
	);
}

export default App;
