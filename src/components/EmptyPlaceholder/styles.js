// @flow

import { StyleSheet } from 'react-native';

import { Typography } from '../../styles/typography';
import { hdp } from '../../utils';
import { Variables } from '../../styles/variables';

export default StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	icon: {
		marginTop: hdp(4.5),
	},
	text: {
		...Typography.Text.MEDIUM,
		marginTop: Variables.SpacingVertical.EXTRA_LARGE,
		textAlign: 'center',
	},
});
