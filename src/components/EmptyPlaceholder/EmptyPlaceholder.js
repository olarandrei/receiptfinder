// @flow

import * as React from 'react';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { View, Text } from 'react-native';

import styles from './styles';

import { Colors } from '../../styles/colors';
import { wdp } from '../../utils';

type Props = {};

function EmptyPlaceholder() {
	return (
		<View style={styles.container}>
			<MaterialCommunityIcon style={styles.icon} name="arrow-up" color={Colors.PRIMARY} size={wdp(12)} />
			<Text style={styles.text}>Please use the above input to search for a recipe.</Text>
		</View>
	);
}

export default React.memo<Props>(EmptyPlaceholder);
