// @flow

import React from 'react';
import { shallow } from 'enzyme';

import EmptyPlaceholder from './EmptyPlaceholder';

describe('EmptyPlaceholder component tests', () => {
	it('Snapshot tests an EmptyPlaceholder component', () => {
		const wrapper = shallow(<EmptyPlaceholder />);
		expect(wrapper).toMatchSnapshot();
	});
});
