// @flow

import React from 'react';
import { shallow } from 'enzyme';

import RecipeDetails from './RecipeDetails';

describe('RecipeDetails component tests', () => {
	const recipe = {
		title: 'Recipe',
		href: 'https://google.com',
		ingredients: 'ingredient1, ingredient2, ingredient3',
	};

	it('Snapshot tests an RecipeDetails component', () => {
		const wrapper = shallow(<RecipeDetails recipe={recipe} />);
		expect(wrapper).toMatchSnapshot();
	});
});
