// @flow

import { StyleSheet } from 'react-native';

import { Colors } from '../../styles/colors';
import { Typography } from '../../styles/typography';
import { Variables } from '../../styles/variables';

export default StyleSheet.create({
	container: {
		flex: 1,
		marginTop: Variables.SpacingVertical.EXTRA_LARGE,
		justifyContent: 'space-between',
	},
	title: {
		...Typography.Text.MEDIUM,
		textAlign: 'center',
	},
	list: {
		marginTop: Variables.SpacingVertical.EXTRA_LARGE,
	},
	item_container: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		padding: Variables.SpacingHorizontal.TINY,
	},
	item_bullet: {
		fontSize: Variables.FontSize.EXTRA_SMALL,
		color: Colors.PRIMARY,
		marginRight: Variables.SpacingHorizontal.EXTRA_SMALL,
	},
	button: {
		backgroundColor: Colors.PRIMARY,
		borderRadius: 5,
		paddingHorizontal: Variables.SpacingHorizontal.EXTRA_SMALL,
		paddingVertical: Variables.SpacingHorizontal.MEDIUM,
	},
	button_text: {
		...Typography.Text.MEDIUM,
		color: Colors.WHITE,
		textAlign: 'center',
	},
});
