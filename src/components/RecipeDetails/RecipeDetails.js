// @flow

import * as React from 'react';
import { View, Text, FlatList, TouchableOpacity, Linking } from 'react-native';

import styles from './styles';

import type { Recipe } from '../../types';
import { Typography } from '../../styles/typography';

type Props = {
	recipe: Recipe,
};

function RecipeDetails(props: Props) {
	const ingredients = props.recipe.ingredients.split(',').map(item => item.trim());

	const renderItem = React.useCallback((params: { item: string }) => {
		return (
			<View style={styles.item_container}>
				<Text style={styles.item_bullet}>{'\u2B24'}</Text>
				<Text style={Typography.Text.MEDIUM}>{`${params.item.charAt(0).toUpperCase() + params.item.slice(1)}`}</Text>
			</View>
		);
	}, []);

	const keyExtractor = React.useCallback((item: string) => item, []);

	const onPress = React.useCallback(() => {
		Linking.canOpenURL(props.recipe.href).then(() => Linking.openURL(props.recipe.href));
	}, [props.recipe.href]);

	return (
		<View style={styles.container}>
			<View>
				<Text style={styles.title}>{`Number of ingredients needed: ${ingredients.length}`}</Text>

				<FlatList
					data={ingredients}
					style={styles.list}
					renderItem={renderItem}
					numColumns={2}
					keyExtractor={keyExtractor}
				/>
			</View>

			{props.recipe.href && (
				<TouchableOpacity style={styles.button} onPress={onPress}>
					<Text style={styles.button_text}>Open Recipe</Text>
				</TouchableOpacity>
			)}
		</View>
	);
}

export default React.memo<Props>(RecipeDetails);
