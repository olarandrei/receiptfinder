// @flow

import { StyleSheet} from 'react-native';

import { Colors } from '../../styles/colors';
import { Typography } from '../../styles/typography';
import { Variables } from '../../styles/variables';

export default StyleSheet.create({
	header: {
		backgroundColor: Colors.PRIMARY,
		paddingVertical: Variables.SpacingVertical.EXTRA_SMALL,
	},
	header_text: {
		...Typography.Text.LARGE,
		color: Colors.WHITE,
		textAlign: 'center',
	},
});
