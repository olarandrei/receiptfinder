// @flow

import * as React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

type Props = {};

function Header() {
	return (
		<View style={styles.header}>
			<Text style={styles.header_text}>Recipe Finder</Text>
		</View>
	);
}

export default React.memo<Props>(Header);
