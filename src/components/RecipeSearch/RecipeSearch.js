// @flow

import * as React from 'react';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { Modal, View, Text, TouchableOpacity, SafeAreaView, TextInput, FlatList } from 'react-native';

import styles from './styles';

import type { Recipe } from '../../types';

import { Colors } from '../../styles/colors';
import { doNetworkRequest, wdp } from '../../utils';
import { Typography } from '../../styles/typography';
import { Variables } from '../../styles/variables';

type LoadMoreProps = {
	visible: boolean,
	onPress: () => void,
};

function LoadMore(props: LoadMoreProps) {
	return props.visible && (
		<TouchableOpacity style={styles.item_container} onPress={props.onPress}>
			<Text style={styles.item_more_text}>Load More Recipes</Text>
		</TouchableOpacity>
	);
}

type SelectDialogProps = {
	visible: boolean,
	onRequestClose: () => void,
	onSelect: (value: Recipe) => void,
};

function SelectDialog(props: SelectDialogProps) {
	const { visible, onRequestClose, onSelect } = props;

	const [searchText, setSearchText] = React.useState('');
	const [recipes, setRecipes] = React.useState<Array<Recipe>>([]);
	const [page, setPage] = React.useState(1);
	const [isError, setError] = React.useState(false);

	const renderItem = React.useCallback((params: { item: Recipe }) => {
		return (
			<TouchableOpacity style={styles.item_container} onPress={() => onSelect(params.item)}>
				<Text style={Typography.Text.MEDIUM}>{params.item.title}</Text>
			</TouchableOpacity>
		);
	}, [onSelect]);

	const keyExtractor = React.useCallback((item: Recipe, index: number) => `${item.title}-${index}`, []);

	const onLoadMore = React.useCallback(() => {
		setError(false);
		doNetworkRequest(`http://www.recipepuppy.com/api/?q=${searchText}&p=${page + 1}`)
			.then(res => {
				setRecipes(recipes.concat(res.results));
				setPage(page + 1);
			})
			.catch(() => {
				setRecipes([]);
				setError(true);
			});
	}, [page, recipes, searchText]);

	React.useEffect(() => {
		if (searchText.length >= 2) {
			const timeoutID = setTimeout(() => {
				setError(false);
				doNetworkRequest(`http://www.recipepuppy.com/api/?q=${searchText}&p=1`)
					.then(res => setRecipes(res.results))
					.catch(() => setError(true));
			}, 500);

			return () => clearTimeout(timeoutID);
		}
	}, [searchText]);

	return (
		<Modal visible={visible} onRequestClose={onRequestClose} animationType="slide" transparent>
			<SafeAreaView style={styles.dialog_container}>
				<TouchableOpacity style={styles.dialog_back} onPress={onRequestClose}>
					<MaterialCommunityIcon
						style={styles.dialog_back_icon}
						name="chevron-left"
						size={20}
						color={Colors.BLACK}
					/>
					<Text style={Typography.Text.MEDIUM}>Back</Text>
				</TouchableOpacity>

				<View style={styles.search_input_container}>
					<TextInput
						autoFocus
						autoCorrect={false}
						placeholder="Search for Recipe"
						style={styles.search_input}
						onChangeText={setSearchText}
						value={searchText}
					/>
					<MaterialIcon
						name="search"
						size={Variables.IconSize.LARGE}
						color={Colors.BLACK}
					/>
				</View>

				{isError ? (
					<View style={styles.error_container}>
						<MaterialIcon name="error-outline" color={Colors.PRIMARY} size={wdp(12)} />
						<Text style={styles.error_text}>Whoops. Something went wrong, please try again :(</Text>
					</View>
				) : (
					<FlatList
						data={recipes}
						style={styles.list}
						renderItem={renderItem}
						keyExtractor={keyExtractor}
						keyboardShouldPersistTaps="always"
						ListFooterComponent={<LoadMore onPress={onLoadMore} visible={recipes.length !== 0} />}
					/>
				)}
			</SafeAreaView>
		</Modal>
	);
}

type RecipeSearchProps = {
	onRecipeSelect: (recipe: Recipe) => void,
};

function RecipeSearch(props: RecipeSearchProps) {
	const { onRecipeSelect } = props;

	const [isVisible, setVisible] = React.useState(false);
	const [label, setLabel] = React.useState('Search for Recipe');

	const openDialog = React.useCallback(() => setVisible(true), []);
	const closeDialog = React.useCallback(() => setVisible(false), []);

	const onSelect = React.useCallback((value: Recipe) => {
		onRecipeSelect(value);
		setLabel(value.title);
		closeDialog();
	}, [closeDialog, onRecipeSelect]);

	return (
		<TouchableOpacity style={styles.select_input} onPress={openDialog}>
			<View style={styles.select_input_content}>
				<Text style={Typography.Text.MEDIUM} numberOfLines={1}>
					{label}
				</Text>

				<MaterialCommunityIcon name="chevron-right" size={Variables.IconSize.LARGE} color={Colors.BLACK} />
			</View>

			<SelectDialog visible={isVisible} onRequestClose={closeDialog} onSelect={onSelect} />
		</TouchableOpacity>
	);
}

export default React.memo<RecipeSearchProps>(RecipeSearch);
