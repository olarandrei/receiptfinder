// @flow

import React from 'react';
import { shallow } from 'enzyme';

import RecipeSearch from './RecipeSearch';

describe('RecipeSearch component tests', () => {
	it('should select a value', () => {
		const onRecipeSelectSpy = jest.fn();

		const wrapper = shallow(<RecipeSearch onRecipeSelect={onRecipeSelectSpy} />);

		expect(
			wrapper
				.find('Text')
				.contains('Search for Recipe')
		).toBe(true);

		wrapper
			.find('SelectDialog')
			.first()
			.props()
			.onSelect({
				title: 'Recipe',
				href: 'https://gogole.com',
				ingredients: 'ingredient1, ingredient2, ingredient3',
			});

		expect(onRecipeSelectSpy).toHaveBeenCalled();
		expect(onRecipeSelectSpy).toHaveBeenCalledWith({
			title: 'Recipe',
			href: 'https://gogole.com',
			ingredients: 'ingredient1, ingredient2, ingredient3',
		});
	});
});
