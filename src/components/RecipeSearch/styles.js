// @flow

import { StyleSheet} from 'react-native';

import { Colors } from '../../styles/colors';
import { Typography } from '../../styles/typography';
import { Variables } from '../../styles/variables';

export default StyleSheet.create({
	select_input: {
		backgroundColor: Colors.GRAY,
		borderRadius: 5,
		paddingVertical: Variables.SpacingVertical.MEDIUM,
		paddingHorizontal: Variables.SpacingHorizontal.EXTRA_SMALL,
	},
	select_input_content: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
	},
	dialog_container: {
		flex: 1,
		backgroundColor: Colors.WHITE,
	},
	dialog_back: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	dialog_back_icon: {
		marginLeft: Variables.SpacingHorizontal.MEDIUM,
		marginRight: Variables.SpacingHorizontal.TINY,
		marginVertical: Variables.SpacingHorizontal.EXTRA_LARGE,
	},
	search_input_container: {
		backgroundColor: Colors.GRAY,
		flexDirection: 'row',
		paddingHorizontal: Variables.SpacingHorizontal.EXTRA_SMALL,
		paddingVertical: Variables.SpacingVertical.MEDIUM,
		borderRadius: 5,
		marginHorizontal: Variables.SpacingHorizontal.MEDIUM,
	},
	search_input: {
		flex: 1,
		...Typography.Text.MEDIUM,
	},
	list: {
		marginTop: Variables.SpacingVertical.EXTRA_LARGE,
	},
	item_container: {
		paddingHorizontal: Variables.SpacingHorizontal.EXTRA_LARGE,
		paddingVertical: Variables.SpacingVertical.MEDIUM,
		borderTopWidth: 1,
		borderTopColor: Colors.GRAY,
	},
	item_more_text: {
		...Typography.Text.MEDIUM,
		color: Colors.PRIMARY,
		textAlign: 'center',
	},
	error_container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		marginHorizontal: Variables.SpacingHorizontal.MEDIUM,
	},
	error_text: {
		...Typography.Text.MEDIUM,
		textAlign: 'center',
		marginTop: Variables.SpacingVertical.EXTRA_LARGE,
	},
});
